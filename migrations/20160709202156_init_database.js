
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('posts', function(table) {
      table.increments();
      table.string('title', 50).notNullable();
      table.text('content').nullable();
      table.timestamp('createdAt');
      table.timestamp('updatedAt');
    }),
    knex.schema.createTable('comments', function(table) {
      table.increments();
      table.text('content').notNullable();

      // post의 id를 저장할 칼럼
      table
        .integer('postId', 11)
        .unsigned();

      table.timestamp('createdAt');
      table.timestamp('updatedAt');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('posts'),
    knex.schema.dropTable('comments')
  ]);
};
