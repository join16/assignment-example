
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('comments', function(table) {
      table
        .foreign('postId')
        .references('id')
        .inTable('posts');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('comments', function(table) {
      table.dropForeign('postId');
    })
  ]);
};