'use strict';

module.exports = {

  // test 환경 db config
  test: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      database: 'devlog_assignment_test',
      user:     'root',
      password: null
    }
  },
  
  // 개발환경에서 실행할 db config
  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      database: 'devlog_assignment_development',
      user:     'root',
      password: null
    }
  }

};