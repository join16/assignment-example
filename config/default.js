'use strict';

// application 관련 설졍 값들 (config module 사용)

exports.port = 3000;

exports.apiPrefix = '/api';

// node 실행환경 (default: development)
exports.nodeEnv = process.env.NODE_ENV || 'development';

exports.sessionSecretKey = 'anyRandomStringForSession';

exports.CONSTANTS = require('./additional/constants');

// db 관련 config 를 exports 함
exports.database = require('./additional/database')[exports.nodeEnv];

exports.apiServerUrl = 'http://localhost:3000/api';