'use strict';

var Promise = require('bluebird');
var request = require('superagent');
var config = require('config');

var apiBaseUrl = config.get('apiServerUrl');

/**
 * url로 api 호출. "get" method 만 가능
 * 
 * @param {String} url
 */
exports.callApi = function callApi(url) {
  return new Promise(function(resolve, reject) {
    request
      .get(apiBaseUrl + url)
      .end(function(err, res) {
        if (err) {
          reject(err);
        } else {
          resolve(res.body);
        }
      });
  });
};