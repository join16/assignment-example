'use strict';

var _ = require('lodash');
var config = require('config');

var nodeEnv = config.get('nodeEnv');

/**
 * @fileoverview exporess.js의 res object extend 하는 미들웨어
 *
 * 이 middleware 뒤의 함수들에서는 여기에서 res object에 추가한 함수 & 값들을 사용할 수 있음
 * 여기에서 res 객체에 추가한 경우 이름 앞에 $를 붙임
 */

module.exports = function responseExtender(req, res, next) {

  /**
   * res.send 함수의 extension.
   * 응답값을 보내기 전에 공통적으로 처리해야 할 로직들을 처리한 후 res.send 함수를 호출한다.
   *
   * @param {*} data
   *
   * @return {void}
   */
  res.$sendSuccess = function $sendSuccess(data) {

    // 응답하기 전에 처리해야 할 로직들  ex) 응답 결과 로그 남기기, 불필요한 정보 필터링 등등

    // data가 undefined일 경우, 에러로 처리
    if (_.isUndefined(data)) {
      var err = new Error('Undefined data has passed');
      return next(err);
    }
    // null인 data가 넘어왔을 경우, { success: true } 를 응답값으로 보냄
    if (_.isNull(data)) {
      data = { success: true };
    }
    
    // .toJSON 함수가 정의되어 있을 경우 실행한다 (bookshelf 관련 오브젝트일 경우)
    if (_.isFunction(data.toJSON)) {
      data = data.toJSON();
    }
    
    res.status(200).send(data);
  };
  
  res.$sendError = function $sendError(err) {
    
    // 에러 응답하기 전에 처리해야 할 로직들
    
    // 에러 응답값 필터링
    var errorResponse = {
      status: err.status || 500,
      message: err.message,
      errors: err.errors
    };
    
    // 실서버 환경일 경우, 에러 디버깅 메세지 필터링
    if (nodeEnv === 'production') {
      errorResponse.errors = null;
    }
    
    res.status(errorResponse.status).send(errorResponse);
  };
  
  /**
   * status와 message 값을 가진 error 오브젝트를 리턴
   * 이 에러가 error handler 미들웨어로 넘어가면 설정된 status를 http status code로 보냄
   * 
   * @param {Number} status
   * @param {String} message
   * 
   * @return {Error}
   */
  res.$createResponseError = function $createResponseError(status, message) {
    var err = new Error(message);
    err.status = status;
    
    return err;
  };

  // extension이 끝난 후 다음 middleware 호출
  next();
};

//////////