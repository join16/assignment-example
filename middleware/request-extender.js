'use strict';

/**
 * @fileoverview exporess.js의 req object extend 하는 미들웨어
 *
 * 이 middleware 뒤의 함수들에서는 여기에서 req object에 추가한 함수 & 값들을 사용할 수 있음
 * 여기에서 res 객체에 추가한 경우 이름 앞에 $를 붙임
 */

module.exports = function requestExtender(req, res, next) {
  
  // request context 상에서 사용할 오브젝트
  // ex) postId에 해당하는 post를 찾은 후 req.$data.post 에 해당 값을 저장하는 용도로 사용
  req.$data = {};

  // extension 완료 후 다음 middleware 호출
  next();
};