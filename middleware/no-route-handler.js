'use strict';

module.exports = function noRouteHandler(req, res, next) {
  var err = res.$createResponseError(404, 'No route matches');
  next(err);
};