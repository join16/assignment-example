'use strict';

var expressValidation = require('express-validation');

module.exports = function errorHandler(err, req, res, next) {
  
  // validation error일 경우
  if (err instanceof expressValidation.ValidationError) {
    err.message = err.statusText;
  }
  
  res.$sendError(err);
};