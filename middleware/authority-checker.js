'use strict';

var config = require('config');

var AUTHORITIES = config.get('CONSTANTS.AUTHORITIES');

module.exports = {

  user: function userAuthorityChecker(req, res, next) {
    var authority = req.session.authority;

    if (authority !== AUTHORITIES.USER) {
      var err = res.$createResponseError(403, 'Only user can access');
      return next(err);
    }
    
    // 로그인된 유저일 경우, 다음 미들웨어로 넘김
    next();
  }

};