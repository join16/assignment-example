'use strict';

var _ = require('lodash');

var arrayUtils = {
  findElement: findElement,
  getNextId: getNextId
};

module.exports = arrayUtils;


/**
 * arrayFindFunc에서 true 값을 리턴하는 element를 가져옴. 매칭되는 element가 없을 경우 null 리턴
 * 
 * example)
 * var arr = [{ id: 1}, { id: 2 }];
 * findElement(arr, function(element) {
 *   return element.id === 2;
 * }); // 2번째 element 리턴
 * 
 * @param {Array} arr
 * @param {Function} arrayFindFunc 찾고자 하는 element일 경우 true 리턴하는 함수
 * 
 * @return {*} matched element or null
 */
function findElement(arr, arrayFindFunc) {
  var i = _.findIndex(arr, arrayFindFunc);
  
  return (i >= 0) ?
      arr[i] :
      null;
}

function getNextId(arr) {
  var lastElement = arr[arr.length - 1];
  return lastElement.id + 1;
}