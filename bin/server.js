'use strict';

var config = require('config');

var app = require('../app');

var port = config.get('port');

app.listen(port, function() {
  console.log('Server listening at port ' + port);
});