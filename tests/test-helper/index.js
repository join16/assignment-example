'use strict';

var dbTestHelper = require('./db-test-helper');
var e2eTestHelper = require('./e2e-test-helper');

var testHelper = {
  db: dbTestHelper,
  e2e: e2eTestHelper
};

module.exports = testHelper;