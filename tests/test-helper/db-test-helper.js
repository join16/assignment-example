'use strict';

var Promise = require('bluebird');
var config = require('config');
var _ = require('lodash');

var fs = require('fs');
var path = require('path');

var db = require('../../models');

var fixturesPath = path.join(__dirname, '../fixtures');
var knex = db.bookshelf.knex;

// knex에서 migration 관리용으로 사용하는 테이블들 (자동으로 관리)
var knexMigrationTables = ['knex_migrations', 'knex_migrations_lock'];

var dbTestHelper = {
  
  setupTestDatabase: function() {
    // 테스트 환경이 아닌 상황에서 실행되었을 때 에러 발생시킴 (안전장치)
    if (config.get('nodeEnv') !== 'test') {
      throw new Error('db test helper method has called in non test environment');
    }
    
    return _dropAllTables()
      .then(function() {
        return _runAllMigrations();
      })
      .then(function() {
        return _insertFixtures();
      });
  }

};

// exports `dbTestHelper`
module.exports = dbTestHelper;


///// private 함수들 (private 함수들은 이름 앞에 _를 붙여준다 (가독성을 위해))

function _dropAllTables() {
  // knexMigrationTables 어레이 복사
  var tables = _.clone(knexMigrationTables);
  
  _.forIn(db, function(Model) {
    if (_.isObject(Model.prototype) && _.isString(Model.prototype.tableName)) {
      tables.push(Model.prototype.tableName);
    }
  });
  
  return Promise.all(tables.map(function(table) {
    return knex.schema.dropTableIfExists(table);
  }));
}

function _runAllMigrations() {
  return knex.migrate.latest();
}

function _setForeignKeyCheck(trx, enable) {
  var value = enable ?
    '1' :
    '0';

  return trx.raw('SET `foreign_key_checks`=' + value);
}

function _insertFixtures() {
  // fixtures 디렉토리의 fixture 파일들을 가지고 테이블에 데이터 삽입

  var insertPromises = [];

  // foreign key 문제 때문에 테스트 데이터를 삽입하는 과정을 하나의 transaction으로 처리함
  // 1. test 데이터들을 삽입하기 위해 foreign key check를 disable한다. 
  //    (enable되어 있으면 foreign key에 해당하는 id를 가진 row가 없을 경우 에러를 발생시킴)
  // 2. fixtures/ 디렉토리에 저장된 json 파일들을 전부 file 이름에 해당하는 테이블에 insert한다.
  // 3. insertion이 끝나면 foreign key check를 다시 enable 해준다.
  return knex.transaction(function(trx) {
    return _setForeignKeyCheck(trx, false)
      .then(function() {
        fs
          .readdirSync(fixturesPath)
          .forEach(function(file) {
            // 확장자 제거
            var tableName = file.replace('.json', '');
            var data = require(path.join(fixturesPath, file));

            // fixture 데이터들 한번에 삽입
            insertPromises.push(trx.batchInsert(tableName, data));
          });

        return Promise.all(insertPromises);
      })
      .then(function() {
        return _setForeignKeyCheck(trx, true);
      });
  });
}