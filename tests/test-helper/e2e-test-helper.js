'use strict';

var request = require('supertest');

var app = require('../../app');

var e2eTestHelper = {
  request: request(app)
};

module.exports = e2eTestHelper;