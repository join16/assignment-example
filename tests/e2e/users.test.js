'use strict';

var expect = require('chai').expect;

var testHelper = require('../test-helper');
var db = require('../../models');

var request = testHelper.e2e.request;

describe('e2e: users api', function() {
  
  before(function(done) {
    testHelper.db
      .setupTestDatabase()
      .then(function() {
        done();
      })
      .catch(done);
  });
  
  describe('POST /api/users', function() {
    
    it('creates user with hashed password', function(done) {
      request
        .post('/api/users')
        .send({
          account: 'user1234',
          password: 'user123'
        })
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.not.include.keys(['hashedPassword', 'password']);

          new db.User({ id: res.body.id })
            .fetch()
            .then(function(user) {
              expect(user.get('hashedPassword')).to.not.equal('user123');
              done();
            })
            .catch(done);
        })
    });
    
  });
  
});