'use strict';

var expect = require('chai').expect;

var testHelper = require('../test-helper');

var request = testHelper.e2e.request;

describe('e2e: posts api', function() {

  // 테스트 수행하기 전에 test db setup 해줌
  before(function(done) {
    testHelper.db
      .setupTestDatabase()
      .then(function() {
        done();
      })
      .catch(done);
  });

  describe('GET /api/posts', function() {

    it('should return posts array', function(done) {
      request
        .get('/api/posts')
        .expect(200)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }

          var body = res.body;
          // response body는 posts 어레이여야 함
          expect(body).to.be.a('array').and.to.have.length(2);
          done();
        });
    });

  });

});