

# Javascript Style Guide
<br/>

일관성 있고 가독성 좋은 javascript 코드를 위한 스타일 가이드입니다. <br/>
스타일 가이드를 너무나도 읽기 싫어하는 여러분들을 위해 필수적인 내용들만을 추렸답니다. <br/>
본 프로젝트에 들어가기 전에 한번 읽어주세요ㅠㅠ <br/>

### Case

- 모든 변수명 & 함수명은 `camelCase` 를 사용한다.
- Class 이름은 `PascalCase` 를 사용한다.
- 상수 이름은 `UPPER_CASE` 를 사용한다.

```js
// bad
var some_variable = 1234;
function some_class() {}

// good
var someVaraible = 1234;
function someMethod() {}
function SomeClass() {}
var MAX_USER_NUMBER = 100000;
```

### Space & New Line

- 항상 2 space만큼 들여쓰기 한다.
- tab 캐릭터 대신 (\t) soft tab (tab 입력 시 whitespace로 indent 하는 것. 보통 에디터에서 설정 가능)를 이용한다.
- 줄바꿈은 UNIX 스타일의 \n 을 이용한다. 윈도우를 사용한다면 Git에서 지원하는 \r\n -> \n 자동 변환 기능을 이용한다.

```js
// bad
function bad() {
    var hello = 'ddd';
}

// good
function good() {
  var hello = 'good example';
}

```

- 파일 뒤의 의미없는 공백들을 잘 정리하자.
- `if`, `for` `function` `switch` 등 `()` 사이에는 한칸 띄어준다. 
- 단, 함수 이름과 `(` 사이에는 띄어쓰기를 넣지 않는다.

```js
// bad
if(condition){}
for(var i = 0;i<10;i++) {}
var a = function() {}

// good
if (condition) {}
for (var i = 0; i < 10; i++) {}
var a = function () {}

// good
var a = function foo() {}
```

### Semicolon

- js는 문법적으로 semicolon 없이도 잘 작동한다. 그렇지만 아직 많은 개발자들이 semicolon을 사용하는 것에 익숙하므로 끝에 semicolon을 붙여주도록 하자.

### 100 Characters per Line

- 횡으로 스크롤해서 봐야하는 코드만큼 보기 힘든 코드도 없다. 화면을 가로로 분할하여 사용하거나 작은 모니터를 사용하는 개발자들을 배려하여 
코드 한 줄에 100자를 넘기지 않도록 한다.

### String

- 홑따옴표를 사용한다.
- 빈 스트링을 만들 때에도 `String` 생성자 대신 `''` 을 이용한다. (String 생성자 이용하지 말자)
- 한 스트링이 너무 길어진다면 `+` 를 이용하여 나눈 후 다음 줄로 넘겨주자.
- * 주의: `+`로 나누는 걸 너무 많이 사용하면 성능에 영향을 미침 (적절히 사용하자)
- 보통 코드 한 줄이 100자를 넘길 것 같을 경우 스트링을 + 로 나눠주면 됨

```js
// bad
var str1 = new String();
var str2 = 'this is very very long string this is very very long string ...';

// good
var str1 = '';
var str2 = 'this is very very long string' +
  'this is very very long string' +
  'this is very very long string';
```

### Brace

- 조건문, 함수 등이 한줄이더라도 `{}`를 생략하지 말고 사용한다.
- `{` 는 같은 라인에서 연다.

```js
// bad
if (condition) doSomething();

if (condition)
	doSomething();

if (condition)
{
	doSomething();
}

for (var i = 0; i < 10; i++) doSomething();

// good
if (condition) {
	doSomething();
}
for (var i = 0; i < 10; i++) {
	doSomething();
}

if (condition1) {
	// do something
} else if (condition2) {
	// ...
} else {
	// ...
}
```

### Object

- `Object` 생성자 대신 literal syntax (`{}`)를 이용한다.
- key에 reserved word를 사용하지 않는다. (ex. default, class). 대신, 비슷한 의미의 동의어를 쓴다.

```js
// bad
var obj = {
	default: 'something'
};

// good
var obj = {
	defaultData: 'something'
};
```

- object 초기화
	+ 한 줄에서 초기화할 경우 { 뒤에, } 앞에 한 칸씩 띄어준다.
	+ 여러 줄에 걸쳐 초기화할 경우, 한 줄당 `key: value`를 선언한다.
	+ 짧은 초기화문이 아닐 경우, 일반적으로 여러 줄에 걸쳐서 초기화하는 것을 선호한다.

```js
// bad
var obj = {name:'John'};

// good
var obj = { name: 'John' };

// bad
var obj = { key1: 'key1', key2: 'key2',
	key3: 'key3',
	key4: 'key4';
};

// good
var obj = {
	key1: 'key1',
	key2: 'key2',
	key3: 'key3'
};
```

- `:` 정렬은 하지 않는다.

```js
// bad
var obj = {
	key1					 : 'key1',
	longKey 			 : 'longKey',
	veryVeryLongKey: 'hahahaha'
};
```

- key는 literal하게 (`''` 없이) 선언한다. literal하게 선언할 수 없을 경우에만 스트링으로 선언한다.

```js
// good
var obj = {
	key1: 'key1',
	key2: 'key2'
};

// good
var obj = {
	key1: 'key1',
	goodExample: true,
	'another-key': 'aaa',
	'/users/1/posts': 'api'
};

// bad
var obj = {
	'key1': 'key1'
};
```

- property 접근
	+ property에 접근할 때는 `.`를 이용하여 literal하게 접근한다.
	+ `.`을 이용할 수 없거나, 변수로 property에 접근하는 경우에만 []를 이용한다

```js

// good
var a = obj.key1;
var b = obj['another-key'];

// good
var key = 'key1';
var a = obj[key];

// bad
var a = obj['key1'];

```

- key는 camelCase를 따르도록 하며, `''`를 이용하는 경우는 최대한 사용하지 않도록 한다. (어쩔 수 없을 경우에만 사용)

### Array

- `Array` 생성자 대신 literal syntax (`[]`)를 이용한다.
- `[` 뒤나 `]` 앞에 띄어쓰기를 넣지 않는다.
- `,` 뒤에 한 칸씩 띄어준다.

```js
// bad
var arr = new Array(1, 2, 3);
var arr = [ 1, 2, 3 ];

// good
var arr = [1, 2, 3];
```

- object array 일 경우 올바른 방식

```js
// good
var arr = [{
	name: 'John',
	age: 1
}, {
	name: 'James',
	age: 2
}, {
	name: 'Alice',
	age: 10
}];
```

- `for` 문을 사용할 경우, array.length 를 조건에 직접 사용하지 않고 변수로 할당하여 사용한다. 이는 혹시 모를 무한 루프를 방지하기 위함이다.

```js
// bad
for (var i = 0; i < arr.length; i++) {
	// if arr.push has called, it never ends.
}

// good
var length = arr.length;
for (var i = 0; i < length; i++) {
	// ...
}

```

### Operator

- 기본적으로 모든 operator 앞, 뒤로 한 칸씩 띄어준다.
- unary operator (`++`, `--`) 같은 경우는 띄어주지 않는다.

```js
// bad
var result=a+b;

// good
var result = a + b;

// bad
i ++;

// good
i++;
```

- 항상 `==`, `!=` 대신 `===`, `!==`를 이용한다.

```js
// bad
if (name == 'John') {
	// ...
}

// good
if (name === 'John') {
	// ...
}
```

### Function

- 함수 이름은 readable하게, 함수 이름과 argument만 보고도 어떠한 기능을 하는 지 예상할 수 있게 한다.

```js

// very bad (read is very ambiguous)
function read(val1, val2) {
	// ...
}

// good
function readFile(sourcePath, encoding) {
	// ...
}
```

- `function` 과 함수 이름 사이에는 한 칸 띄어쓴다.
- 익명 함수 (anonymous function) 보다는 함수에 이름을 정의해 주는 것을 선호한다. (Error 발생 시 추적이 용이해짐)

```js
// good (but below one is better)
app.use(function (req, res, next) {});

// better
app.use(function createPost(req, res, next) {});
```

- 한 라인 안에서 선언, 호출을 끝내는 것을 원칙으로 한다. 단, 한 줄 당 글자 수 제한을 넘어갈 경우에는 아래와 같은 방식으로 다음 줄로 넘긴다.

```js
// declaration
function someVeryVeryLongFunction(thisIsOneOfVeryVeryLongArgument1, 
																	thisIsOneOfVeryVeryLongArgument1, 
																	thisIsOneOfVeryVeryLongArgument1) {
  // some logic here
}

// function call

// good
doSomething(
	val1, va2, va3
);

// also good
doSomething(
	val1,
	val2,
	val3
);
```

- private 함수인 경우에는 함수 이름 앞에 _를 붙여주도록 하자. 이는 다른 개발자들에게 알려주기 위함이다.

```js
// good
function _somePrivateFunction() {
	// ...
}
```

- 너무 많은 로직을 처리하는 함수는 가독성을 저해한다. 함수가 너무 커질 경우 기능 별로 다른 함수로 분리한다.
- 함수 내에서 가능하면 일찍 반환한다. 조건문, 함수 등의 중첩을 줄일 수 있다.

```js
// not bad, but below one is preferred
function findUserById(users, id) {
	var user = null;

	if (typeof id === 'number') {
		var length = users.length;

		for (var i = 0; i < length; i++) {
			if (users[i].id === id) {
				user = users[i];
				break;
			}
		}

		return user;
	} else {
		return null;
	}
}

// preferred
function findUserById(users, id) {
	if (typeof id !== 'number') {
		return null;
	}

	for (var i = 0; i < users.length; i++) {
		if (users[i].id === id) {
			return user;
		}
	}
}

```

### Variables

- 한 줄당 하나의 변수를 선언한다.
- 여러 줄에 걸쳐 변수를 선언할 경우, `,`를 사용하지 말고 모든 줄에 `var`를 써주도록 한다.
- 변수는 필요한 곳에서 선언해서 쓴다. 꼭 스코프의 상단에서 모두 선언할 필요는 없다.

```js
// bad
var value1 = 1,
    value2 = 2,
    value3 = 3;

// good
var value1 = 1;
var value2 = 2;
var value3 = 3;
```

- private 변수 이름 앞에는 _를 붙여주도록 하자.

```js
// good
var _privateVariable = 1;

var obj = {
	name: 'John',
	age: 20,
	_privateData: {
		// ...
	}
};
```

### Condition

- 사용할 수 있다면 짧은 조건문(shorcut)을 이용한다.
	+ **Object** === true
	+ **Undefined** === false
	+ **Null** === false
	+ **Boolean**: 그 값 그대로
	+ **Number**: `+0`, `-0`, `NaN` === false, 나머지는 모두 true
	+ **String**: `''` === false, 나머지는 모두 true

```js
var name = '';

// bad
if (name === '') {}

// good
if (name) {}
```

- 명시적이지 않은 복잡한 조건문일 경우, 따로 설명적인 이름의 변수를 선언하여 이에 할당한다.

```js
// bad
if (/^[A-Za-z0-9]+\@.*\.[a-z]+$/.test(account)) {}

// good
var isValidEmail = /^[A-Za-z0-9]+\@.*\.[a-z]+$/.test(account);
if (isValidEmail) {}
```

### Block 나누기 (New Line)

- 코드의 문맥, 기능에 따라 적절하게 block을 나눠주도록 한다. 이 때, empty line은 한 줄만 넣는다.
- Tip: 일반적으로 크게 변수 선언, 메인 로직, 리턴 부분 세 부분으로 나눈다.

```js
// good
var aa = 1;
var bb = 2;
var cc = 3;

doSomething(aa);
doSomething(bb);
doSomethingElse(cc);

return aa;
```

### Ternary Operator (삼항 연산자)

- 적절한 삼항 연산자는 가독성을 좋게 한다. 적절한 상황에서는 적극 활용하도록 하자.
- 중첩되거나 `{}`으로 묶이는 삼항 연산자는 코드를 엉망으로 만든다. 절대 사용하지 않도록 한다.
- 삼항 연산자는 여러 줄에 걸쳐서 사용한다.

```js
// not bad, but below one is better
var result;
if (a === b) {
	result = 1;
} else {
	result = 2;
}

// better
var result = (a === b) ?
	1 :
	2;

// bad
var result = (a === b) ? 1 : 2;

// very very bad. never use it.
var result = (a === b) ?
	(c === d) ?
		1 :
		2 :
	4;

// very very bad. never use it.
var result = (a === b) ? 
	{
		a: 1,
		b: 2
	} :
	null;
```

### module, require

- `require` 는 3 block으로 나눈다.
	+ **1st block**: npm install로 설치한 외부 모듈들
	+ **2nd block**: node core 모듈
	+ **3rd block**: 프로젝트 내의 파일들

```js
var _ = require('lodash');
var expressSession = require('express-session');
var express = require('express');

var http = require('http');
var fs = require('fs');
var path = require('path');

var foo = require('./foo');
var bar = require('./bar');

```

- exports는 파일 상단에, 변수 선언부분 다음 block에 정의한다.

```js
var express = require('express');
var config = require('config');

var fs = require('fs');
var path = require('path');

var foo = require('./foo');

var port = config.get('port');
var foobar = foo.bar;
var app = express();

module.exports = app;

app.use(foo.middleware1);
// ...
```

### Method Chaining

- method chaining은 여러 줄에 걸쳐서 적는다. 한 줄에는 하나의 method가 오도록 한다.
- method chaining을 사용할 경우 indentation을 해준다.

```js
// bad
User.where({ id: 1 }).fetch().then(function(user) {

}).catch(function(err) {

});

// bad
User
.where({ id: 1 })
.fetch()
.then(function(user) {

})
.catch(function(err) {

});

// good
User
	.where({ id: 1 })
	.fetch()
	.then(function(user) {

	})
	.catch(function(err) {

	});
```

### Native Prototype

- 절대로 javascript 내장 오브젝트의 prototype을 확장하지 않는다. 이는 후에 의도와 다르게 동작하는 코드를 방지하기 위함이다.

```js
// bad and dangerous
Array.prototype.isEmpty = function () {
	return !this.length;
};

// good
function isEmpty(arr) {
	return !arr.length;
}
```

### Callback

- Node.js에서 callback의 첫 번째 인자는 error를 받는다. 이는 Node.js의 convention이다. error가 발생하지 않은 경우 null이 넘어온다.

```js
fs.readFile('./foo.txt', function(err, content) {
	if (err) {
		// error handling
	}
});
```

### ||

- `||` 는 default 연산자 라고도 불린다. 초기값을 설정해 줄 때 사용하면 보다 깔끔한 코드를 작성할 수 있다.

```js
// not bad, but below one is better.
function findUserById(id) {
	if (!id) {
		id = 1;
	}
}

// better
function findUserById(id) {
	id = id || 1;
}
```

### 주석

- 가장 좋은 코드는 주석 없이도 직관적으로 이해할 수 있는 코드다.
- 가능한 주석을 줄이되, 아래와 같은 경우에는 주석을 작성해주도록 한다.
	+ 함수에 대한 parameter, return, 기능 설명
	+ class 및 class property에 대한 설명
	+ 다소 난해하거나 복잡한 로직을 처리할 경우, 처리할 로직에 대한 간단한 한줄 주석을 달아준다.


- 주석의 문법은 기본적으로 JSDoc 문법을 따른다. [JsDoc 문법](http://usejsdoc.org/)
- 한번 쭉 읽어보면 좋으나, 전부 읽어보기 귀찮다면 함수, class 에 대한 주석 정도만이라도 읽어보자

### 한 줄 주석

- 설명하고자 하는 코드 윗 줄에 `//`를 이용하여 작성한다.
- 한 줄 주석 위에 한 줄 띄어준다.

```js
var a = 1;
var b = 2;

// a + b를 수행
var c = a + b;
```

### multi line comment

- multi line 주석은 위에서 언급한 대로 JsDoc의 문법을 따른다.
- `/**/` 를 이용한다.
- 함수에 대한 주석 예시

```js
/**
 * Returns sum of a and b.
 * 
 * @param {Number} a first operand
 * @param {Number} b second operand
 * @return {Number} result of a + b
 */
function sum(a, b) {
	return a + b;
}
```

- class 주석 예시

```js
/**
 * Constructor of MyClass.
 * This class is just for explanation.
 * 
 * @param {Object} params
 * @param {Number} params.foo
 * @param {Boolean} [params.isOptional] this is optional parameter
 * @constructor
 */
function MyClass(params) {

}
```
