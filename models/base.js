'use strict';

var config = require('config');
var knex = require('knex')(config.get('database'));
var bookshelf = require('bookshelf')(knex);

bookshelf.plugin('virtuals');
bookshelf.plugin('visibility');

var BaseModel = bookshelf.Model.extend({
  hasTimestamps: ['createdAt', 'updatedAt']
});

var BaseCollection = bookshelf.Collection.extend();

exports.BaseModel = BaseModel;
exports.BaseCollection = BaseCollection;
exports.bookshelf = bookshelf;