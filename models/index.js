'use strict';

var _ = require('lodash');

var fs = require('fs');
var path = require('path');

// 모델들이 저장된 디렉토리 = 현재 index.js이 있는 폴더
var modelsPath = __dirname;

var db = {};

// exports `db`
module.exports = db;

fs
  .readdirSync(modelsPath)
  .filter(function(file) {
    // 파일이름이 index.js (현재파일)이 아니고 확장자가 .js인 파일들만 필터링
    return /^.*\.js$/.test(file) && (file !== 'index.js');
  })
  .forEach(function(file) {

    // 현재디렉토리/파일이름.js 를 require 해옴
    var definitions = require(path.join(modelsPath, file));

    // 불러온 값들을 db 오브젝트에 추가함
    _.assign(db, definitions);
  });