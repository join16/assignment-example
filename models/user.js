'use strict';

var Promise = require('bluebird');
var bcrypt = require('bcrypt-nodejs');

var base = require('./base');
var db = require('./');

var User = base.BaseModel.extend({
  tableName: 'users',

  virtuals: {
    password: {
      get: function() {
        return null;
      },
      set: function(value) {
        this.set('hashedPassword', value);
      }
    }
  },

  hidden: ['hashedPassword', 'password']
});

User.prototype.on('saving', function(user, attrs, options) {
  var password = user.get('hashedPassword');

  return new Promise(function(resolve, reject) {
    bcrypt.hash(password, null, null, function(err, hash) {
      if (err) {
        return reject(err);
      }

      user.set('hashedPassword', hash);
      resolve();
    });
  });
});

var Users = base.BaseCollection.extend({
  model: User
});

exports.User = User;
exports.Users = Users;