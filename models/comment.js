'use strict';

var base = require('./base');
var db = require('./');

var Comment = base.BaseModel.extend({
  tableName: 'comments',
  
  post: function() {
    return this.belongsTo(db.Post, 'postId');
  }
});

var Comments = base.BaseCollection.extend({
  model: Comment
});

exports.Comment = Comment;
exports.Comments = Comments;