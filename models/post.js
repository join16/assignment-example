'use strict';

var base = require('./base');
var db = require('./');

var Post = base.BaseModel.extend({
  tableName: 'posts',

  // comments의 postId 칼럼을 이용해서 hasMany 관계 지정
  comments: function() {
    return this.hasMany(db.Comment, 'postId');
  }
});

var Posts = base.BaseCollection.extend({
  model: Post
});

exports.Post = Post;
exports.Posts = Posts;