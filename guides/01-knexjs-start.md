
# 01. Knex.js 시작하기

### 1. 모듈 설치

```sh
$ npm install knex -g
$ npm install knex mysql --save
```

- npm install knex -g: knex 모듈을 글로벌로 설치함 (command line interface)
	인스톨 후 터미널에서 `knex` 명령어를 사용 가능

	```sh
	$ knex --help
	```

	도움말 출력되면 정상 설치된 거임


### 2. knexfile 초기화

```sh
$ knex init
```

- 위의 명령어로 프로젝트 루트 디렉토리에 knexfile.js 를 생성
	+ knexfile.js 는 knex cli (command line interface) 에서 사용하는 db 설정파일

- 우리는 코드에서 사용할 db 설정파일을 cli에서 사용할 db 설정파일을 한 파일에서 관리할 것임
	+ config/database.js 를 만들어서 db connection 관련 설정을 해준다

- config/default.js 에 database.js 설정 파일을 require 한 후 exports 해준다

- knexfile.js에서 db 설정파일을 config 모듈에서 불러온다
	+ (node config 모듈 https://github.com/lorenwest/node-config 참고)

### 3. database 만들기

```sh
$ mysql -uroot
> create database devlog_assignment_test;
> create database devlog_assignment_development;
```

- config/database.js 에서 test, development 환경에서 지정해 준 database를 <br/>mysql 명령어를 이용하여 만들어준다
- knex cli, 우리 코드에서 실행 환경을 지정해줌으로써 사용할 database를 선택할 수 있음 (default = development)
- NODE_ENV=development npm start 같이 명령어 실행 앞에 NODE_ENV={환경 이름} 을 넣어줘서 실행 환경 설정


### 4. posts, comments 테이블 만들기

- 앞으로 database의 스키마는 javascript 코드로 관리할 것임
	+ 그래야 다른 사람들과 변경되는 db 스키마를 공유할 수 있음
	+ 그리고 js 코드 (knex.js) 를 이용해서 관리하는 게 더 쉬움

- db의 스키마 (구조) 변경사항들을 migration이라 부르는 파일로 관리함
	+ 테이블을 생성하고, 컬럼을 추가하는 등의 db 구조를 변경해주는 js 코드들을 마이그레이션 파일에 작성함
	+ 마이그레이션 파일들은 migrations/ 디렉토리에다가 모아서 저장할 것임

```sh
$ knex migrate:make init_database
```

- 다음 명령어를 실행하여 migrations/ 디렉토리와 {마이그레이션 고유 id}_init_database.js 파일 생성

- exports.up 은 해당 마이그레이션 파일을 적용하였을 때에 실행될 함수.

- exports.down은 혹시 해당 마이그레이션에 문제가 있을 경우 기존의 버전으로 rollback 할 때 호출될 함수.

- migrations/{...}_init_database.js 처럼 마이그레이션 파일을 작성
	+ exports.up은 posts와 comments 테이블을 생성하는 함수
	+ exports.down은 위의 테이블 생성 마이그레이션을 롤백할 때 호출될 함수. (테이블 제거)

- table 생성 시에, 일반적으로 id, createdAt, updatedAt 컬럼은 필수적으로 생성한다.
	+ id 는 row를 판별하는 primary key로 사용
	+ createdAt 는 해당 row의 생성된 시간을 기록하는 칼럼 (knex에서 자동으로 관리해줌)
	+ updatedAt 는 해당 row가 마지막으로 업데이트된 시간을 기록 (knex에서 자동으로 관리)

```sh
$ knex migrate:latest
```

- 위의 명령어로 마이그레이션 실행. (posts, comments 테이블이 생성됨)
	+ devlog_assignment_development 테이블 확인



