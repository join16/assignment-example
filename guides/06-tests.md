
# 6. 테스트 작성하기

### 1. 테스트 헬퍼 작성하기

- 자동화 테스트를 작성하기 위해서는 먼저 테스트를 실행할 가상의 환경을 만들어야 함
- NODE_ENV=test 를 테스트 실행하는 명령어 앞에 넘겨줌으로써 테스트 환경에서 모든 코드들을 실행해야 한다.
- 자동화 테스트를 위해 필요한 부분들을 먼저 생각을 해보자.
	+ 테스트 환경의 DB: database 초기화 -> migrations 파일들 실행 (최신의 db 스키마로 만듦) -> 테스트용 가상의 데이터 db에 삽입
	+ 테스트 환경의 서버: e2e test에 필요. 테스트 코드에서 테스트 서버에 요청을 보내보고 예상되는 응답값이 오는 지 확인해야 하기 때문에 필요

### 2. 테스트 DB setup 하는 db test helper 구현하기

- 우선적으로 테스트 환경의 DB를 구현하는 코드를 작성해보자. (tests/test-helper/db-test-helper.js) 파일 참고
- `setupTestDatabase` 함수를 호출하면 db 안의 모든 테이블들을 drop하고, migrations 실행하고, test data를 넣어준다.
- 코드에서 사용된 용어들 정리
	+ fixture: `모형` 이라는 뜻의 단어. 테스트 환경에서 사용할 가상의 데이터들을 부르는 단어임
	+ transaction: database에서 사용되는 단어. 여러 개의 query를 마치 하나의 query처럼 처리할 수 있게 해준다.
		* 여러 개의 query를 처리할 때에 중간의 query에서 에러가 발생했을 경우 query 몇개는 실행되고 몇개는 실행되지 않는 문제 발생
		* 위와 같은 문제를 해결하기 위해 사용되는 개념. 한 transaction 안에서 실행된 query들은 모든 query들이 완료된 후에 DB에 적용된다.
		* query가 실행되면 대기 상태에 있다가, 모든 query들이 다 실행되면 그제서야 모든 처리 결과를 실제로 DB에 적용한다.
		* 자세한 내용은 mysql transaction 으로 검색해보자.
- `_insertFixtures` 함수에서는 fixture 데이터 삽입 시 발생할 수 있는 `foreignKeyConstraint error`를 처리하기 위해 트랜잭션 사용
- `foreignKeyConstraint error`는 foreign key column가 참조하는 row가 실제로 존재하지 않는 경우에 발생하는 에러
- 위 코드에서는 posts 와 comments를 동시에 삽입하기 때문에, comments가 삽입될 당시 postId에 해당하는 post가 존재하지 않을 수 있음
- 이를 해결하기 위해 transaction을 사용하고, transaction의 시작에서 mysql의 `foreign_key_checks` 값을 0으로 설정해준다.

### 3. 테스트 환경의 서버 관련 e2e test helper 구현하기

- 테스트 환경의 서버에 요청을 보내볼 수 있게 하기 위해, `supertest` 라는 모듈을 사용할 것임
- 모듈: https://github.com/visionmedia/supertest (읽어봅시다)

```sh
$ npm install supertest --save-dev
```

- 위의 명령어를 통해 supertest 모듈을 dev dependency로 설치
- dev dependency는 개발환경에서만 설치할 모듈들. 주로 테스트, 디버깅 용도의 모듈들
- 구현은 tests/test-helper/e2e-test-helper.js 파일 참고

```js
var testHelper = require('./test-helper');
var request = testHelper.e2e.request;

request.get('/api/posts');
// ...
```

- 위와 같은 방식으로 사용할 수 있음. 자동으로 app.js로 서버를 실행시키고, 그 서버로 해당 url과 method로 요청을 보내줌



### 4. mocha.js & chai.js 설치하기

- mocha.js는 테스트 코드를 실행시켜주는 모듈이고, chai.js 는 mocha 안에서 결과값이 예상대로 나오는 지를 테스트할 수 있게 도와주는 모듈

```sh

$ npm install mocha -g
$ npm install chai --save-dev

```

- 위의 명령어를 통해 mocha cli를 설치하고, chai.js 모듈을 dev dependency로 설치

- mocha와 chai.js에서 BDD style 의 함수들을 보도록 하자.
	+ BDD는 Behavior Driven Development의 약자로, 어떤 함수나 api가 어떠한 Behavior를 해야 하는 지를 위주로 테스트하는 방식을 말함

- tests/e2e/posts.e2e.js 파일을 참고해서 테스트 코드를 작성해보도록 하자
	+ 파일 이름 뒤의 `.test.js` 부분은 테스트 파일임을 나타내기 위해 내가 그냥 이름을 그렇게 지은거임
	+ 취향대로 `posts-test.js` 나 `posts.js` 등 어떻게 이름 지어도 무방함

- 테스트 코드를 다 작성한 다음에 아래 명령어를 실행해서 테스트를 돌려보자.

```sh
$ NODE_ENV=test mocha {테스트 파일 경로}
```
- 테스트 결과를 확인하고, test가 fail하였을 경우 코드나 테스트를 적절히 수정해서 모든 test가 passed 되도록 하자.


### 5. npm test 명령어 추가하기

- package.json 의 scripts 영역에 "start" 부분에서 `npm start`가 실행할 명령어 `node bin/server.js` 를 지정해놨음
- 마찬가지로 `npm test` 실행시 (또는 `npm run test`) 모든 테스트들을 실행하도록 스크립트를 지정해보자.

```json
{
	"scripts": {
		"start": "node bin/server.js",
		"test": "NODE_ENV=test mocha tests/**/*.test.js"
	}
}
```

- 위와 같이 tests/ 디렉토리 안에 있는 모든 *.test.js 파일들을 mocha를 통해 실행하는 스크립트를 추가하자.
- `tests/*.test.js` tests/posts.test.js 같이 tests/ 디렉토리에 있는 파일들만 실행함
- `tests/**/*.test.js` 로 지정해주면 tests/ 안의 디렉토리들을 재귀적으로 방문해서 test/ 디렉토리 안에 있는 모든 *.test.js 파일들을 실행함
- package.json 에 test 명령어 추가 후 아래 명령어로 전체 테스트를 실행해보자

```sh
$ npm test
```

- package.json 안의 scripts 영역에 등록된 스크립트들은 `npm run {설정한 명령어 이름}` 으로 실행할 수 있음
- 위와 같이 test에 스크립트를 설정해 준 경우 `npm run test` 로 실행할 수 있음
- `start, stop, test` 등 몇몇 명령어들은 named 명령어로써, `npm run {명령어 이름}`이 아니라 바로 `npm {명령어 이름}` 으로 실행 가능


