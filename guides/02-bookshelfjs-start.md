
# 2. Bookshelf.js 로 ORM 구현 시작하기

### 1. BaseModel, BaseCollection 만들기

- 모델 파일들을 저장할 models/ 디렉토리를 만든다
- 모든 모델에서 공통적으로 사용할 기능들을 models/base.js 라는 파일에다가 정의할 것임
- Model 클래스로 생성된 오브젝트 하나는 table에서 가져온 row 하나에 대응되고, Collection 오브젝트는 Model의 array라고 생각하자
- BaseCollection은 지금은 extend해줄 기능이 없지만, 나중을 대비해 일단은 선언해놓자

### 2. Post, Comment 모델 정의하기

- models/post.js, models/comment.js 참고
- tableName에 아까 만들었던 posts, comments 테이블을 지정해줌으로써 테이블과 모델을 매핑해준다

### 3. models 의 엔트리파일 만들어주기

- 앞으로 모델이 많아질텐데, 사용할 모델들을 매번 require하기 번거로움
- 그래서 require 한번으로 모든 모델들을 사용할 수 있게 models 디렉토리에 엔트리파일을 만들어주자
- models/index.js 참고
	+ fs.readdirSync 함수를 이용해서 models 폴더에 있는 모든 파일 이름들을 어레이로 가져옴
	+ 가져온 파일 이름을 가지고 require해서 파일을 불러온다.
	+ post.js, comment.js 에서 exports 한 Model과 Collection들을 db 오브젝트에 추가한다.

- 이후 model들을 사용할 때 
	```js
		var db = require('./models');
		db.Post // Post 모델
		db.Comment // Comment 모델
	```
	위와 같이 한번만 require해서 모든 모델들을 사용할 수 있음
	