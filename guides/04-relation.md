
# 04. ORM Relation 사용하기

### 1. Model에 realtion 지정하기

- models/Post.js, models/Comment.js 참고
- Post hasMany Comment 이고, Comment belongsTo Post 이다
- 위의 두 relation은 comments 테이블의 postId 칼럼을 이용함

### 2. router 에서 related 데이터 같이 가져오기

- routes/posts.js 참고
- /api/posts/:postId 에서 post가 가지고 있는 comments를 같이 가져올 것임
- fetch() 함수에서 `withRelated` 옵션을 이용해서 hasMany 관계에 있는 comments를 같이 불러옴
- 연관관계에 있는 데이터 같이 불러오기 => 정말 쉬움