
# 5. foreign key index 추가하기

- 다른 테이블을 참조할 때 쓰이는 foreign key (앞에서 만들었던 postId)에 db 단에서 index를 걸어놓으면 <br/> db 처리 속도가 빨라짐
- index를 걸어주는 migration 파일을 작성하자

```sh
$ knex migrate:make add_index_to_post_id
```

- 위의 명령어를 실행해 (파일 이름은 자유) 마이그레이션 파일을 생성하자
- migrations/{...}_add_index_to_post_id.js를 참고해 postId에 index를 걸어주는 마이그레이션 파일을 작성하자

```sh
$ knex migrate:latest
```

- 마이그레이션 파일 작성 후 위의 명령어를 통해 마이그레이션을 실행하자