'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');
var config = require('config');
var expressValidation = require('express-validation');
var expressHandlebars = require('express-handlebars');

var requestExtender = require('./middleware/request-extender');
var responseExtender = require('./middleware/response-extender');
var noRouteHandler = require('./middleware/no-route-handler');
var errorHandler = require('./middleware/error-handler');
var routes = require('./routes');

var sessionSecretKey = config.get('sessionSecretKey');
var app = express();
var hbs = expressHandlebars.create({});

// exports `app`
module.exports = app;


// set express validation global options
expressValidation.options({
  allowUnknownBody: false
});

// view engine setting
app.engine('.html.hbs', hbs.engine);
app.set('view engine', '.html.hbs');

// register middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(session({
  secret: sessionSecretKey,
  resave: false,
  saveUninitialized: true
}));

// register custom middleware
app.use(requestExtender);
app.use(responseExtender);

// register routers
app.use(routes);

// no route handler
app.use(noRouteHandler);

// error handler
app.use(errorHandler);