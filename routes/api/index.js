'use strict';

var express = require('express');

// load routers
var posts = require('./posts');
var sessions = require('./sessions');
var users = require('./users');

var router = express.Router();

// exports `router`
module.exports = router;

router.use('/posts', posts);
router.use('/sessions', sessions);
router.use('/users', users);