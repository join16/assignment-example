'use strict';

var express = require('express');
var validate = require('express-validation');
var _ = require('lodash');

var utils = require('../../utils');
var authorityChecker = require('../../middleware/authority-checker');
var postsValidations = require('../../validations/post-validations');
var commentValidations = require('../../validations/comment-validations');
var db = require('../../models');

var router = express.Router();

module.exports = router;

router.post('/', function(req, res, next) {
  new db.User(req.body)
    .save()
    .then(function(user) {
      res.$sendSuccess(user);
    })
    .catch(next);
});