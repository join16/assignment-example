'use strict';

var express = require('express');
var validate = require('express-validation');
var _ = require('lodash');

var utils = require('../../utils');
var authorityChecker = require('../../middleware/authority-checker');
var postsValidations = require('../../validations/post-validations');
var commentValidations = require('../../validations/comment-validations');
var db = require('../../models');

var router = express.Router();

module.exports = router;

router.get('/', function(req, res, next) {
  db.Post
    .fetchAll()
    .then(function(posts) {
      // posts는 Posts 콜렉션의 오브젝트
      res.$sendSuccess(posts);
    })
    .catch(next);
});

// router에 등록된 순서대로 함수 호출
// 1. validate() 에서 리턴한 함수
// 2. 그 다음에 등록한 createPost 함수
router.post('/',
  authorityChecker.user,
  validate(postsValidations.createPost),
  function createPost(req, res, next) {
    var body = req.body;

    var post = new db.Post(body);

    post
      .save()
      .then(function() {
        res.$sendSuccess(post);
      })
      .catch(next);
  });

// :postId parameter가 넘어왔을 경우
router.param('postId',
  function(req, res, next, postId) {
    // db.Post.forge() 는 new db.Post() 와 똑같음

    db.Post
      .forge({ id: postId })
      .fetch({ withRelated: ['comments'] })
      .then(function(post) {

        // post가 null일 경우 (postId에 해당하는 post가 없을 경우)
        if (_.isNull(post)) {
          throw res.$createResponseError(404, 'Post not found');
        }

        // 뒤에서 사용할 수 있게 req.$data에 post를 추가
        req.$data.post = post;
        next();
      })
      .catch(next);
  });

router.get('/:postId', function(req, res) {
  var post = req.$data.post;
  
  res.$sendSuccess(post);
});

router.put('/:postId',
  authorityChecker.user,
  validate(postsValidations.updatePost),
  function(req, res, next) {
    var body = req.body;
    var post = req.$data.post;

    post.set(body);

    post
      .save()
      .then(function() {
        res.$sendSuccess(post);
      })
      .catch(next);
  });

router.delete('/:postId',
  authorityChecker.user,
  function(req, res, next) {
    var post = req.$data.post;

    post
      .destroy()
      .then(function() {
        res.$sendSuccess(null);
      })
      .catch(next);
  });


///// 라우터 /posts/:postId/comments

router.get('/:postId/comments', function(req, res, next) {
  var post = req.$data.post;

  db.Comment
    .where({ postId: post.get('id') })
    .fetchAll()
    .then(function(comments) {
      res.$sendSuccess(comments);
    })
    .catch(next);
});

router.post('/:postId/comments',
  validate(commentValidations.createComment),
  function(req, res, next) {
    var body = req.body;
    var post = req.$data.post;

    var comment = new db.Comment({
      content: body.content,
      postId: post.id
    });

    comment
      .save()
      .then(function() {
        res.$sendSuccess(comment);
      })
      .catch(next);
  });

router.param('commentId', function(req, res, next, commentId) {

  db.Comment
    .forge({ id: commentId })
    .fetch()
    .then(function(comment) {

      if (_.isNull(comment)) {
        throw res.$createResponseError(404, 'Comment not found');
      }

      req.$data.comment = comment;

      next();
    })
    .catch(next);
});

router.get('/:postId/comments/:commentId', function(req, res) {
  res.$sendSuccess(req.$data.comment);
});

router.put('/:postId/comments/:commentId',
  validate(commentValidations.updateComment),
  function(req, res, next) {
    var body = req.body;
    var comment = req.$data.comment;

    comment.set(body);

    comment
      .save()
      .then(function() {
        res.$sendSuccess(comment);
      })
      .catch(next);
  });

router.delete('/:postId/comments/:commentId', function(req, res, next) {
  var comment = req.$data.comment;
  
  comment
    .destory()
    .then(function() {
      res.$sendSuccess(null);
    })
    .catch(next);
});