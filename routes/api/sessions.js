'use strict';

var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var validate = require('express-validation');
var config = require('config');

var sessionValidations = require('../../validations/session-validations');
var data = require('../../data');

var AUTHORITIES = config.get('CONSTANTS.AUTHORITIES');
var user = data.user;
var router = express.Router();

// exports `router`
module.exports = router;

router.post('/',
    validate(sessionValidations.createSession),
    function(req, res, next) {
      var body = req.body;

      // account가 틀림
      if (body.account !== user.account) {
        var err = res.$createResponseError(401, 'Invalid account');
        next(err);
        return;
      }

      bcrypt.compare(body.password, user.hashedPassword, function(err, isMatched) {
        // unexpected error
        if (err) {
          return next(err);
        }

        if (!isMatched) {
          var invalidPasswordError = res.$createResponseError(401, 'Invalid password');
          return next(invalidPasswordError);
        }

        // session에 hasAuthorized = true로 설정
        req.session.authority = AUTHORITIES.USER;

        req.session.save(function(err) {
          if (err) {
            return next(err);
          }
          
          // session 저장 후 send response
          res.$sendSuccess(null);
        });
      });
    });