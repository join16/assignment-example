'use strict';

var express = require('express');

var apiRequest = require('../lib/api-request');

var router = express.Router();

module.exports = router;

router.get('/', function(req, res, next) {
  apiRequest
    .callApi('/posts')
    .then(function(posts) {
      res.render('posts', { posts: posts });
    })
    .catch(next);
});