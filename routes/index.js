'use strict';

var express = require('express');
var config = require('config');

var api = require('./api');
var posts = require('./posts');

var router = express.Router();

module.exports = router;

router.use(config.get('apiPrefix'), api);
router.use('/posts', posts);