'use strict';

var Joi = require('joi');

exports.createSession = {
  body: {
    account: Joi.string(),
    password: Joi.string()
  }
};