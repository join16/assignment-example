'use strict';

var Joi = require('joi');

exports.createPost = {
  body: {
    title: Joi.string().min(5).max(20).required(),
    content: Joi.string()
  }
};

exports.updatePost = {
  body: {
    title: Joi.string().min(5).max(20),
    content: Joi.string()
  }
};