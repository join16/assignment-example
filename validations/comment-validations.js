'use strict';

var Joi = require('joi');

exports.createComment = {
  body: {
    content: Joi.string().min(1).max(50).required()
  }
};

exports.updateComment = {
  body: {
    content: Joi.string().min(1).max(50)
  }
};